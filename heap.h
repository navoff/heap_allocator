/**
 * @file heap.h
 * @author Постнов В.М.
 * @date 26 янв. 2020 г.
 * @brief
 * @addtogroup
 * @{
*/

#ifndef HEAP_H_
#define HEAP_H_

#include <stdint.h>

// ======================================= Definition ==============================================

typedef struct h_header_s
{
  uint16_t size;
  uint16_t is_free;
  struct h_header_s *next;
  struct h_header_s *prev;
} h_header_t;

typedef struct
{
  h_header_t *header;
} h_footer_t;

typedef struct
{
  h_header_t *first_free;
  uint32_t begin;
  uint32_t end;
  uint16_t free_size;
} heap_t;

#define HEAP_CHUNK_OVERHEAD             (sizeof(h_header_t) + sizeof(h_footer_t))

// ======================================= Declaration =============================================

void h_init(heap_t *heap, void *memory, uint16_t size);
void *h_alloc(heap_t *heap, uint16_t size);
void h_free(heap_t *heap, void *p);
uint32_t h_avail(heap_t *heap);

#endif /* HEAP_H_ */

/** @} */
