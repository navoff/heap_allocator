/**
 * @file heap.c
 * @author Постнов В.М.
 * @date 26 янв. 2020 г.
 * @brief
 * @addtogroup
 * @{
*/

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "heap.h"

// ========================================= Definition ============================================

// ========================================= Declaration ===========================================

void h_remove_from_list(h_header_t *header);
void h_replace_in_list(h_header_t *old, h_header_t *new);
h_footer_t *h_find_footer(h_header_t *header);
void h_create_footer(h_header_t *header);

// ======================================== Implementation =========================================

void h_init(heap_t *heap, void *memory, uint16_t size)
{
  memset(memory, 0, size);
  heap->begin = (uint32_t)memory;
  heap->end = (uint32_t)memory + size;

  heap->first_free = (void *)memory;
  heap->first_free->size = size - HEAP_CHUNK_OVERHEAD;
  heap->free_size = heap->first_free->size;
  heap->first_free->next = NULL;
  heap->first_free->prev = NULL;
  heap->first_free->is_free = 1;

  h_footer_t *f = h_find_footer(heap->first_free);
  assert((uint32_t)f == (uint32_t)memory + size - sizeof(h_footer_t));
  f->header = heap->first_free;
}

void *h_alloc(heap_t *heap, uint16_t size)
{
  h_header_t *h = heap->first_free;
  h_header_t *found = NULL;
  while (h != NULL)
  {
    if (h->size >= size)
    {
      found = h;
      break;
    }
    h = h->next;
  }

  if (found == NULL)
  {
    return NULL;
  }

  // если оставшегося места достаточно для организации еще одного chunk-а
  if ((found->size - size) >= HEAP_CHUNK_OVERHEAD)
  {
    // разбиваем найденый chunk на два
    h_header_t *split =
        (void *)((uint32_t)found + HEAP_CHUNK_OVERHEAD + size);
    split->size = found->size - size - HEAP_CHUNK_OVERHEAD;
    split->is_free = 1;
    h_create_footer(split);

    found->size = size;
    found->is_free = 0;
    h_create_footer(found);

    h_replace_in_list(found, split);
    if (heap->first_free == found)
    {
      heap->first_free = split;
    }
  }
  else // найденный chunk будет занят полностью
  {
    if (heap->first_free == found)
    {
      heap->first_free = found->next;
    }

    found->is_free = 0;
    h_create_footer(found);
    h_remove_from_list(found);
  }

  heap->free_size -= found->size + HEAP_CHUNK_OVERHEAD;
  return &found->next;
}

void h_free(heap_t *heap, void *p)
{
  h_header_t *header = (void *)((uint8_t *)p - offsetof(h_header_t, next));

  h_header_t *prev = ((h_footer_t *)((uint8_t *)header - sizeof(h_footer_t)))->header;
  h_header_t *next = (void *)((uint8_t *)h_find_footer(header) + sizeof(h_footer_t));

  // если предыдущий chunk вообще существует
  if ((uint32_t)prev >= heap->begin)
  {
    if (prev->is_free)
    {
      // объединяем возвращаемый chunk с предыдущим
      prev->size += header->size + HEAP_CHUNK_OVERHEAD;
      h_create_footer(prev);

      heap->free_size += header->size + HEAP_CHUNK_OVERHEAD;
      return;
    }
  }

  // если следующий chunk вообще существует
  if ((uint32_t)next < heap->end)
  {
    if (next->is_free)
    {
      // объединяем возвращаемый chunk со следующим

      header->is_free = 1;
      header->size += next->size + HEAP_CHUNK_OVERHEAD;
      header->next = next->next;
      header->prev = next->prev;
      h_create_footer(header);
      if (heap->first_free == next)
      {
        heap->first_free = header;
      }

      heap->free_size += next->size + HEAP_CHUNK_OVERHEAD;
      return;
    }
  }

  // если оказались тут, значит соседние chunk-и заняты
  // будем добавлять в начало списка свободных chunk-ов
  header->is_free = 1;

  // если все chunk-и заняты, то возвращаемый будет первым
  if (heap->first_free == NULL)
  {
    heap->first_free = header;
    header->next = NULL;
    header->prev = NULL;
  }
  else
  {
    header->prev = NULL;
    header->next = heap->first_free;
    header->next->prev = header;
    heap->first_free = header;
  }
  heap->free_size += header->size;
}

uint32_t h_avail(heap_t *heap)
{
  return heap->free_size;
}

void h_remove_from_list(h_header_t *header)
{
  if (header->next != NULL)
  {
    header->next->prev = header->prev;
  }

  if (header->prev != NULL)
  {
    header->prev->next = header->next;
  }

  header->next = NULL;
  header->prev = NULL;
}

void h_replace_in_list(h_header_t *old, h_header_t *new)
{
  new->next = old->next;
  new->prev = old->prev;

  old->next = NULL;
  old->prev = NULL;
}

h_footer_t *h_find_footer(h_header_t *header)
{
  uint8_t *footer = (uint8_t *)header;
  footer += HEAP_CHUNK_OVERHEAD - sizeof(h_footer_t);
  footer += header->size;

  return (h_footer_t *)footer;
}

void h_create_footer(h_header_t *header)
{
  h_footer_t *footer = h_find_footer(header);
  footer->header = header;
}

/** @} */
