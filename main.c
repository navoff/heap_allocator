/**
 * @file main.c
 * @author Постнов В.М.
 * @date 26 янв. 2020 г.
 * @brief
 * @addtogroup
 * @{
*/

#include <assert.h>
#include <stdio.h>

#include "heap.h"

// ========================================= Definition ============================================

#define HEAP_MEMORY_SIZE               (100 + HEAP_CHUNK_OVERHEAD)

// ========================================= Declaration ===========================================

void heap_alloc_test(void);
void heap_alloc_free_test(void);

// ======================================== Implementation =========================================

int main(void)
{
  heap_alloc_test();
  heap_alloc_free_test();

  return 0;
}

/**
 * @brief Тестирование функции выделения памяти
 */
void heap_alloc_test(void)
{
  heap_t heap;
  uint8_t heap_memory[HEAP_MEMORY_SIZE];

  // выделить блок допустимого размера
  {
    h_init(&heap, heap_memory, HEAP_MEMORY_SIZE);
    assert(h_avail(&heap) == HEAP_MEMORY_SIZE - HEAP_CHUNK_OVERHEAD);
    void *p = h_alloc(&heap, HEAP_MEMORY_SIZE - HEAP_CHUNK_OVERHEAD);
    assert(p != NULL);
  }

  // выделить 2 соседних блока
  {
    h_init(&heap, heap_memory, HEAP_MEMORY_SIZE);
    void *p1 = h_alloc(&heap, 13);
    void *p2 = h_alloc(&heap, 10);
    assert(((uint32_t)p2 - (uint32_t)p1) == 13 + HEAP_CHUNK_OVERHEAD);
    assert(h_avail(&heap) == HEAP_MEMORY_SIZE - 13 - 10 - 3 * HEAP_CHUNK_OVERHEAD);
  }

  // выделить блок слишком большого размера
  {
    h_init(&heap, heap_memory, HEAP_MEMORY_SIZE);
    void *p = h_alloc(&heap, HEAP_MEMORY_SIZE);
    assert(p == NULL);
  }

  // выделить 2 блока, после чего места не останется
  // выделить 3-ий блок не должно получиться
  {
    h_init(&heap, heap_memory, HEAP_MEMORY_SIZE);
    void *p1 = h_alloc(&heap, HEAP_MEMORY_SIZE - 2 * HEAP_CHUNK_OVERHEAD - 12);
    assert(heap.first_free != NULL);
    assert(p1 != NULL);

    void *p2 = h_alloc(&heap, 10);
    assert(heap.first_free == NULL);
    assert(p2 != NULL);

    void *p3 = h_alloc(&heap, 10);
    assert(p3 == NULL);
  }
}

/**
 * @brief Тестирование функций выделения и освобождения памяти
 */
void heap_alloc_free_test(void)
{
  heap_t heap;
  uint8_t heap_memory[HEAP_MEMORY_SIZE];

  // выделить 3 сегмента
  // вернуть 2-ой сегмент
  // выделить 4-ой сегмент
  // адреса 2-го и 4-го сегментов должны совпадать
  {
    h_init(&heap, heap_memory, HEAP_MEMORY_SIZE);
               h_alloc(&heap, 10);
    void *p2 = h_alloc(&heap, 20);
               h_alloc(&heap, 30);
    h_free(&heap, p2);
    void *p4 = h_alloc(&heap, 5);
    assert(p2 == p4);
  }

  // выделить 3 сегмента
  // вернуть сначала 1-ый потом 2-ой сегменты
  // выделить 4-ой сегмент размером суммы 1-го и 2-го
  // адреса 1-го и 4-го сегментов должны совпадать
  {
    h_init(&heap, heap_memory, HEAP_MEMORY_SIZE);
    void *p1 = h_alloc(&heap, 10);
    void *p2 = h_alloc(&heap, 20);
               h_alloc(&heap, 30);
    h_free(&heap, p1);
    h_free(&heap, p2);
    void *p4 = h_alloc(&heap, 10 + 20);
    assert(p1 == p4);
  }

  // выделить 3 сегмента
  // вернуть сначала 2-ой потом 1-ый сегменты
  // выделить 4-ой сегмент размером суммы 1-го и 2-го
  // адреса 1-го и 4-го сегментов должны совпадать
  {
    h_init(&heap, heap_memory, HEAP_MEMORY_SIZE);
    void *p1 = h_alloc(&heap, 30);
    void *p2 = h_alloc(&heap, 20);
               h_alloc(&heap, 10);
    h_free(&heap, p2);
    h_free(&heap, p1);
    void *p4 = h_alloc(&heap, 30 + 20);
    assert(p1 == p4);
  }

  // выделить 1-ый сегмент максимального размера
  // вернуть 1-ый сегмент
  // выделить 2-ой сегмент
  // адреса 1-го и 2-го сегментов должны совпадать
  {
    h_init(&heap, heap_memory, HEAP_MEMORY_SIZE);
    void *p1 = h_alloc(&heap, HEAP_MEMORY_SIZE - HEAP_CHUNK_OVERHEAD);
    h_free(&heap, p1);
    void *p2 = h_alloc(&heap, 4);
    h_free(&heap, p2);
    assert(p1 == p2);
  }
}

/** @} */
